const form = document.getElementById('auth-form');
const [authButton, esiaButton] = document.querySelectorAll('.main-block-form__btn');
const [loginInput, passwordInput] = document.querySelectorAll('.main-block-form__input');
const errorBlock = document.getElementById('auth-error');
const esiaForm = document.getElementById('esia-form');
let isEsia = false;

esiaButton.addEventListener('click', () => {
  errorBlock.style.display = 'none';
  isEsia = true;
  passwordInput.required = false;
})

authButton.addEventListener('click', () => {
  errorBlock.style.display = 'none';
  passwordInput.required = true;
})

form.addEventListener('submit', (e) => {
  e.preventDefault();
  if (isEsia) {
      const esiaInput = loginInput.cloneNode(true)
      esiaInput.name = 'email';
      esiaForm.append(esiaInput);
      esiaForm.submit();

      esiaButton.disabled = true;
      esiaButton.style.cursor = 'wait';
      authButton.disabled = true;
      authButton.style.cursor = 'wait';
  } else {
    if (/@/.test(loginInput.value)) {
      authGolos();
    } else {
      authSvod();
    }
  }
});

function authSvod() {
  const iframe = document.getElementById('receiveFrame');
  const iframeButton = iframe.contentDocument.getElementById('ButtonLogin');
  iframe.contentDocument.getElementById('TextBoxLogin').value = loginInput.value;
  iframe.contentDocument.getElementById('TextBoxPassword').value = passwordInput.value;
  iframeButton.click();
  authButton.disabled = true;
  authButton.style.cursor = 'wait';
  
  iframe.onload = () => {
    loginInput.value = '';
    passwordInput.value = '';
    authButton.disabled = false;
    authButton.style.cursor = 'pointer';

    if (iframe.contentWindow.location.href === 'http://svod.mgkhrb.ru/svodPlus/selectForm.aspx') {
      return location = 'http://svod.mgkhrb.ru/svodPlus/selectForm.aspx';
    }

    errorBlock.style.display = 'block';
  }
}

async function authGolos() {
  try {
    authButton.disabled = true;
    authButton.style.cursor = 'wait';

    const response = await fetch('https://golos-mgkhrb.bashkortostan.ru/index.php?page=main', {
      method: "POST",
      body: new FormData(form),
    })

    if (response.ok & !response.redirected) {
      form.submit();
    } else {
      errorBlock.style.display = 'block';
      authButton.disabled = false;
      authButton.style.cursor = 'pointer';
    }
    
  } catch (error) {
      errorBlock.style.display = 'block';
      errorBlock.innerHTML = 'Что-то пошло не так.'
      authButton.disabled = false;
      authButton.style.cursor = 'pointer';
  }

  loginInput.value = '';
  passwordInput.value = '';
}

